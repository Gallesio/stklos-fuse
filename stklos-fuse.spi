;;;; -*- coding: utf-8 -*-
;;;;
;;;; stklos-fuse.spi    -- FUSE support for STklos
;;;;
;;;; Copyright © 2007-2018 Erick Gallesio - I3S-CNRS/ESSI <eg@essi.fr>
;;;;
;;;;
;;;; This program is free software; you can redistribute it and/or modify
;;;; it under the terms of the GNU General Public License as published by
;;;; the Free Software Foundation; either version 2 of the License, or
;;;; (at your option) any later version.
;;;;
;;;; This program is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;;; GNU General Public License for more details.
;;;;
;;;; You should have received a copy of the GNU General Public License
;;;; along with this program; if not, write to the Free Software
;;;; Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
;;;; USA.
;;;;
;;;;           Author: Erick Gallesio [eg@essi.fr]
;;;;    Creation date: 10-Jun-2007 19:18 (eg)
;;;; Last file update: 27-Mar-2018 15:45 (eg)
;;;;
(interface stklos-fuse
  (language stklos stdlib)

  (import stklos-posix)         ;; In fact POSIX support is needed by exmaples

  (export
   (fuse-mount args
               (:getattr getattr #f)
               (:readlink readlink #f)
               (:mknod mknod #f)
               (:mkdir mkdir #f)
               (:unlink unlink #f)
               (:rmdir rmdir #f)
               (:symlink symlink #f)
               (:rename rename #f)
               (:link link #f)
               (:chmod chmod #f)
               (:chown chown #f)
               (:truncate truncate #f)
               (:utime utime #f)
               (:open open #f)
               (:read read #f)
               (:write write #f)
               (:statfs statfs #f)
               (:flush flush #f)
               (:release release #f)
               (:fsync fsync #f)
               (:setxattr setxattr #f)
               (:getxattr getxattr #f)
               (:listxattr listxattr #f)
               (:removexattr removexattr #f)
               (:opendir opendir #f)
               (:readdir readdir #f)
               (:releasedir releasedir #f)
               (:fsyncdir fsyncdir #f)
               (:init init #f)
               (:destroy destroy #f))))
